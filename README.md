# Project structure

The project is following the mono repository structure.

Yarn is used as a package manager with his native notion of workspaces to support a mono-repo structure.

Lerna is used for its ability to run scripts on every packages


# Requirements

In order to be run locally, the project needs to be able to access AWS SSM (to dynamically fetch configuration, see README of `@qventura-portfolio/common`).

The AWS CLI can be used to set the credentials (https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html) or the environment variables can be used (`AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`)


# Assumptions

1) GooglePlace API is used to search for the places and get the location.
Without any user inputs (except the initial names provided), we'll rely on the API to get the most accurate locations, and we'll take the first suggestions.
One such example is "The Little Mermaid" returning location for "Little Mermaid The Musical"

2) Non existing places would be filter out and would not appear in the response

3) Distance between locations is currently measured in a dummy way, without taking into consideration the curve of the planet. We all know it's flat anyway.


# Performances

Regarding performances, two things need to be noticed:
1) Lambda as a cold start delay (around 500ms including the network processing time). This happen only the first time the function is called, or when the function expire (The function stays in memory for a while, probably between 5 and 15mn, the exact time being not documented)
2) The ConfigManager, fetching the configuration (GoogleApiKey) has some internal caching, the first time the configuration is fetched, the request will be slightly longer (+- 10ms)
3) The GoogleApiService also has some caching involved. The places are cached using the name send in the request. If the same name is going to be used again, the data is gonna come from the cache and no request to GooglePlaceAPI will be done. This caching is done individually for each location.

For information about the caching being used, see README of `@qventura-portfolio/common`


# Known issues
## Tests using external APIs causing failure
*What*:
The current test are performing calls to external API, including GooglePlacesAPI.
GooglePlacesAPI uses the ip address of the caller to make the results more accurates.
The result of the test could then vary depending of the user running them, causing them to fail

*Solution*:
Mocking the call to the GooglePlacesAPI with `sinon`


# Branching model

A mix of Github Flow and Gitlab Flow will be used for the branching.

https://docs.gitlab.com/ee/workflow/gitlab_flow.html#environment-branches-with-gitlab-flow

Each environment has a corresponding branch, and merging on a branch will trigger a deployment


# Technologies

## Typescript
Typescript will be used as the main language on the project.

## AWS Lambda / Serverless
The APIs will be deployed on AWS Lambda / API Gateway using the Serverless framework


# Packages description

## Common

Contains code that is not domain specific and can be reused in every other package

## Locations Rest API

Expose the service to manage locations through a REST API


# Commands

## General
`yarn build`
Build all the packages and resolve dependencies

`yarn build:clean`
Destroy all the build artifacts

`yarn test`
Run the test on all the packages if the test script is provided

`yarn test:coverage`
Run the coverage script (nyc) on every package if the test:coverage script is provided

## REST APIs

`yarn workspace @qventura-portfolio/xxx start`
Run the function locally on port 5000 using serverless-offline

`yarn workspace @qventura-portfolio/xxx deploy:dev`
Deploy the function on AWS Lambda on the dev environment
