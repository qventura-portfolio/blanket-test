import { Event, Context as AWSContext, Callback } from 'aws-lambda'

import {Logger} from '../lib'

import {
  composeMiddlewares,
  errorHandler,
  initConfigManager,
  initContext,
  initLogger,
  requestLogger
} from '../middlewares'

import Context from './context'

interface LambdaHandlerArgsInterface {
  environment,
  serviceName,
  eventHandler: (context: Context, next: Function) => Promise<void>
}

export default function lambdaHandler(args: LambdaHandlerArgsInterface) {
  return async (event: Event, awsContext: AWSContext, callback: Callback) => {
    // This is require to make AWS Lambda and Mongo work properly
    // See: https://docs.atlas.mongodb.com/best-practices-connecting-to-aws-lambda/
    awsContext.callbackWaitsForEmptyEventLoop = false

    const {
      environment,
      eventHandler,
      serviceName,
    } = args

    const context: Context = {}

    await composeMiddlewares([
      initContext({ event, environment, serviceName }),
      initLogger,
      initConfigManager,
      requestLogger,
      errorHandler,
      eventHandler,
    ]).run(context)

    callback(
      undefined,
      {
        statusCode: context.response.statusCode,
        body: JSON.stringify(context.response.body)
      }
    )
  }
}
