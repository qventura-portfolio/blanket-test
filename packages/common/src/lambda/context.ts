import { Logger } from '../lib'

interface Response {
  body?: any | null
  statusCode?: number
}

interface Request {
  body?: any
  headers?: any
  method?: string
  path?: string
  queryParams?: any
  correlatedRequestId?: string
}

interface Context {
  logger?: Logger
  environment?: string
  serviceName?: string
  response?: Response
  request?: Request
}

export default Context
