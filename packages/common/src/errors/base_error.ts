export default class BaseError extends Error {
  public errorCode
  public statusCode

  constructor({ errorCode, statusCode, message }) {
    super(message)
    this.errorCode = errorCode || 1000
    this.statusCode = statusCode || 500
    this.message = message
  }
}
