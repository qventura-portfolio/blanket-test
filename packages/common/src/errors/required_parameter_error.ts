import BaseError from './base_error'

export default class RequiredParameterError extends BaseError {
  constructor(param) {
    super({
      errorCode: 1101,
      statusCode: 400,
      message: `The parameter ${param} is required`,
    })
  }
}
