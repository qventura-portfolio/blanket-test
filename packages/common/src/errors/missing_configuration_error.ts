import BaseError from './base_error'

export default class MissingConfigurationError extends BaseError {
  constructor(param) {
    super({
      errorCode: 1001,
      statusCode: 500,
      message: `The configuration ${param} is missing`,
    })
  }
}
