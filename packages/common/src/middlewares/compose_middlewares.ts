import compose from 'koa-compose'

const composeMiddlewares = (middlewares) => {
  return {
    run: async (ctx) => {
      const composition = await compose(middlewares)

      return composition(ctx, null)
    }
  }
}

export default composeMiddlewares
