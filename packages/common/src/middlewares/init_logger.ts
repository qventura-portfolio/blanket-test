import {Context} from '../lambda'
import {Logger} from '../lib'

const initLogger = async (ctx: Context, next: Function): Promise<void> => {
  ctx.logger = new Logger({
    serviceName: ctx.serviceName,
    environment: ctx.environment,
    correlatedRequestId: ctx.request.correlatedRequestId,
  })

  await next()
}

export default initLogger
