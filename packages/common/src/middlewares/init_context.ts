import { Event } from 'aws-lambda'

import {Context} from '../lambda'

interface InitContextArgsInterface {
  event: Event
  environment: string
  serviceName: string
}

const initContext = ({ event, environment, serviceName }) => {
  return async(ctx: Context, next: Function): Promise<void> => {
    event.queryStringParameters = {
      ...event.queryStringParameters,
      ...event.multiValueQueryStringParameters,
    }

    ctx.environment = environment
    ctx.serviceName = serviceName

    ctx.request = {
      body: event.body,
      correlatedRequestId: event.requestContext.requestId,
      headers: event.headers,
      method: event.httpMethod,
      path: event.resource,
      queryParams: event.queryStringParameters,
    }

    ctx.response = {
      statusCode: 200
    }

    await next()
  }
}

export default initContext
