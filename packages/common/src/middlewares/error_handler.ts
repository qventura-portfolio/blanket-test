import {Context} from '../lambda'

const errorHandler = async (ctx: Context, next: Function): Promise<void> => {
  try {
    await next()
  } catch(error) {
    ctx.logger.error(error.message, { ...error, stack: error.stack })

    ctx.response.statusCode = error.statusCode || 500

    ctx.response.body = {
      error: {
        errorCode: error.statusCode || 1000,
        message: error.message
      }
    }
  }
}

export default errorHandler
