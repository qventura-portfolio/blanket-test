import {Context} from '../lambda'

const logRequest = async(ctx: Context, next: Function): Promise<void> => {
  const startTime = new Date()
  const startTimeStr = startTime.toISOString()

  const route = {
    path: ctx.request.path,
    method: ctx.request.method
  }

  const request = {
    route,
  }

  const routeString = `${route.method} ${route.path}`

  ctx.logger.info(
    `Request for route: ${routeString}, started at ${startTimeStr}`,
    {
      type: 'request_start',
      request,
      startTime: startTimeStr
    }
  )

  try {
    await next()
  } catch (error) {
    throw error
  } finally {
    const endTime = new Date()
    const endTimeStr = endTime.toISOString()
    const duration = endTime.getTime() - startTime.getTime()

    const response = {
      statusCode: ctx.response.statusCode,
    }

    ctx.logger.info(
        `Request for route: ${routeString}, completed with status ${
            response.statusCode} after ${duration}ms, started at ${
            startTime}, ended at ${endTime}`,
        {
          type: 'request_end',
          request,
          startTime: startTimeStr,
          endTime: endTimeStr,
          duration,
          response,
        })
  }
}

export default logRequest
