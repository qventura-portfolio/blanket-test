import {Context} from '../lambda'
import {ConfigManager} from '../lib'

const initConfigManager = async(ctx: Context, next: Function): Promise<void> => {
  ConfigManager.initInstance({
    environment: ctx.environment,
    serviceName: ctx.serviceName
  })

  await next()
}

export default initConfigManager
