interface CacheEntry {
  value
  expireAt: Date|number
}

interface CachingLayerInterface {
  [key: string]: CacheEntry
}

const caching: CachingLayerInterface = {}

export default class ProcessCaching {
  private static instance: ProcessCaching

  static initInstance() {
    ProcessCaching.instance = new ProcessCaching()
  }

  static getInstance() {
    if (!ProcessCaching.instance) {
      ProcessCaching.initInstance()
    }

    return ProcessCaching.instance
  }

  async getOrRunAndSet(key, run) {
    let entry = this.get(key)

    if(entry == undefined) {
      entry = await run()
      this.set(key, entry)
    }

    return entry
  }

  get(key) {
    const entry: CacheEntry = caching[key]

    if (!entry) {
      return
    }

    if (entry.expireAt < new Date()) {
      this.unset(key)
      return
    }

    return entry.value
  }

  set(key, value, durationInMn = 5) {
    const now = new Date()

    caching[key] = {
      value,
      expireAt: now.setMinutes(now.getMinutes() + durationInMn)
    }
  }

  unset(key) {
    caching[key] = null
  }
}
