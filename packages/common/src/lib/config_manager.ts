import * as AWS from 'aws-sdk'

import {MissingConfigurationError} from '../errors'
import {Logger, ProcessCaching} from '.'

const SSM = new AWS.SSM()

interface ConfigEntry {
  key: string
  value: string
}

interface ServiceUrlEntry {
  name: string
  url: string
}

interface AWSSSMParamsInterface {
  Path: string
  NextToken?: string
}

interface CachingLayer {
  get: Function
  set: Function
  unset: Function
}

interface ConstructorArguments {
  environment: string
  serviceName: string
  cachingLayer?: CachingLayer
}

export default class ConfigManager {
  private environment: string
  private serviceName: string
  private logger: Logger
  private cachingLayer: CachingLayer
  private static instance: ConfigManager

  constructor({ environment, serviceName, cachingLayer = null }) {
    this.cachingLayer = cachingLayer || ProcessCaching.getInstance()
    this.environment = environment
    this.serviceName = serviceName

    this.logger = new Logger({
      serviceName: this.serviceName,
      environment: this.environment
    })
  }

  static initInstance({ environment, serviceName, cachingLayer = null }) {
    ConfigManager.instance = new ConfigManager({ environment, serviceName, cachingLayer })
  }

  static getInstance(context) {
    if(ConfigManager.instance) {
      return ConfigManager.instance
    }

    if (!context) {
      throw new Error('ConfigManager must be initialized first')
    } else {
      ConfigManager.initInstance({
        environment: context.environment,
        serviceName: context.serviceName,
      })
    }

    return ConfigManager.instance
  }

  getEnvVar(key: string, required = true): Promise<string> {
    let fullKey = `/${this.environment}/${this.serviceName}/${key}`

    try {
      return this.get(fullKey, required)
    } catch(error) {
      throw error
    }
  }

  async getEnvVars(): Promise<object> {
    let fullKey
    let keyValueArray = []
    let keyValueDict = {}

    fullKey = `/${this.environment}/${this.serviceName}/`
    keyValueArray = [
      ...keyValueArray,
      ...await this.getAll(fullKey)
    ]

    keyValueDict = {
      ...keyValueDict,
      ...this.fromSSMToDictionnary(keyValueArray, fullKey)
    }

    return keyValueDict
  }

  fromSSMToDictionnary(array, fullKey) {
    const keyValueDict = {}

    array.forEach((keyValuePair) => {
      const key = keyValuePair.key.replace(fullKey, '')
      const value = keyValuePair.value

      keyValueDict[key] = value
    })

    return keyValueDict
  }

  async get(key: string, required = true): Promise<string> {
    this.logger.info(`ConfigManager: Requesting SSM parameter ${key}`)

    const cachedValue: string = this.cachingLayer.get(`ConfigManager#${key}`)

    if (cachedValue) {
      this.logger.info('ConfigManager: Getting value from local cache')

      return cachedValue
    }

    return new Promise<string>((resolve, reject) => {
      SSM.getParameter({Name: key}, (err, data) => {
        if (!err && data.Parameter.Value) {
          const stringValue = data.Parameter.Value as string
          this.cachingLayer.set(`ConfigManager#${key}`, stringValue)

          resolve(stringValue)
        } else if (!err || err.name === 'ParameterNotFound') {
          this.cachingLayer.unset(`ConfigManager#${key}`)

          if (required) {
            reject(new MissingConfigurationError(key))
          } else {
            resolve(null)
          }
        } else {
          this.cachingLayer.unset(`ConfigManager#${key}`)
          reject(err)
        }
      })
    })
  }

  async getAll(startWith: string): Promise<ConfigEntry[]> {
    const cacheKey = `ConfigManager#getAll#${startWith}`
    const params: AWSSSMParamsInterface = {Path: startWith}

    this.logger.info(
        `ConfigManager: Requesting SSM parameters starting with ${startWith}`)

    const cachedValue = this.cachingLayer.get(cacheKey)

    if (cachedValue) {
      this.logger.info('ConfigManager: Getting value from local cache')

      return cachedValue as ConfigEntry[]
    }

    let requestResult
    let parameters: ConfigEntry[] = []

    requestResult = await this.fetchSSMParameters(params)

    while (requestResult) {
      parameters = [
        ...parameters, ...requestResult.Parameters.map((parameter) => {
          return {key: parameter.Name, value: parameter.Value}
        })
      ]

      if (!requestResult.NextToken) {
        break
      }

      params.NextToken = requestResult.NextToken
      requestResult = await this.fetchSSMParameters(params)
    }

    this.cachingLayer.set(`ConfigManager#${cacheKey}`, parameters)

    return parameters
  }

  fetchSSMParameters(params: AWSSSMParamsInterface): Promise<object> {
    return new Promise<object>((resolve, reject) => {
      SSM.getParametersByPath(params, (err, data) => {
        if (!err) {
          const parsedResults = data.Parameters.map((parameter) => {
            return {key: parameter.Name, value: parameter.Value}
          })

          resolve(data)
        } else {
          reject(err)
        }
      })
    })
  }
}
