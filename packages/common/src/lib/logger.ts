import * as winston from 'winston'
const {combine, timestamp, label, printf} = winston.format

interface LoggerInterface {
  serviceName: string
  environment: string
  correlatedRequestId?: string
}

interface InfoInterface {
  timestamp?: Date
  message: string
  level: string
}

export default class Logger {
  private logger

  constructor({
    serviceName,
    environment,
    correlatedRequestId
  } : LoggerInterface) {
    this.logger = winston.createLogger(
      Logger.baseConfiguration({
        serviceName,
        environment,
        correlatedRequestId
      })
    )
  }

  static baseConfiguration({
      serviceName,
      environment,
      correlatedRequestId
    } : LoggerInterface) {
    const commonOptions = Logger.commonTransportOptions({
      serviceName,
      environment,
      correlatedRequestId
    })

    return {
      level: process.env.LOG_LEVEL || 'info',
      transports: [
        new (winston.transports.Console)(commonOptions),
      ]
    }
  }

  static commonTransportOptions({
      serviceName,
      environment,
      correlatedRequestId
    } : LoggerInterface) {

    return {
      silent: environment === 'test',
      timestamp: () => new Date().toISOString(),
      format: combine(
        timestamp(),
        printf(
          info => Logger.formatWithContext({
              serviceName,
              environment,
              correlatedRequestId
            },
            info
          )
        )
      ),
      formatter: (options) => Logger.formatWithContext({
          serviceName,
          environment,
          correlatedRequestId
        },
        options
      )
    }
  }

  error(...args) {
    this.logger.error(...args)
  }

  warning(...args) {
    this.logger.warning(...args)
  }

  info(...args) {
    this.logger.info(...args)
  }

  log(...args) {
    this.logger.log(...args)
  }

  private static formatWithContext({
    serviceName,
    environment,
    correlatedRequestId
  }: LoggerInterface,
      info: InfoInterface) {

    const contextString = Logger.formatLogginContext({
      serviceName,
      environment,
      correlatedRequestId
    })

    const logLevel = Logger.formatLevel(info)
    const sanitizedMessage = Logger.sanitizedMessage(info)
    const sanitizedMetadata = Logger.sanitizedMetadata(info)

    return `[${info.timestamp}]${contextString}[${logLevel}][${sanitizedMessage}][${sanitizedMetadata}]`
  }

  private static formatLogginContext({
    serviceName,
    environment,
    correlatedRequestId
  } : LoggerInterface) {
    return `[${serviceName}][${environment}][${correlatedRequestId}]`
  }

  private static sanitizedMessage(info: InfoInterface) {
    return (info.message || '').replace('\n', '\\n')
  }

  private static sanitizedMetadata(info: InfoInterface) {
    const {timestamp, message, level, ...metadata} = info

    return JSON.stringify(metadata)
  }

  private static formatLevel(info: InfoInterface) {
    return info.level.toUpperCase()[0]
  }
}
