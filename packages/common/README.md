# Lib

## logger

Logger class using winston to process logs.
Logs are formated as follow:
[timestamp][environment][serviceName][correlatedRequestId][logLevel][message][Metadata]

Example:
[2019-07-18T19:55:23.707Z][LocationsService][dev][offlineContext_requestId_7625239394628538][I][Request for route: GET /api/v1/locations, started at 2019-07-18T19:55:23.705Z][{"type":"request_start","request":{"route":{"path":"/api/v1/locations","method":"GET"}},"startTime":"2019-07-18T19:55:23.705Z"}]
[2019-07-18T19:55:23.721Z][LocationsService][dev][offlineContext_requestId_7625239394628538][I][Request for route: GET /api/v1/locations, completed with status 200 after 15ms, started at Thu Jul 18 2019 15:55:23 GMT-0400 (EDT), ended at Thu Jul 18 2019 15:55:23 GMT-0400 (EDT)][{"type":"request_end","request":{"route":{"path":"/api/v1/locations","method":"GET"}},"startTime":"2019-07-18T19:55:23.705Z","endTime":"2019-07-18T19:55:23.720Z","duration":15,"response":{"statusCode":200}}]

Logs formatted as such are collected by CloudWatch and forwarded to ElasticSearch / Kibana

## ConfigManager

ConfigManager is a singleton used to abstract the integration with AWS SSM, which is used to manage configuration.
It uses the `ProcessCaching` to avoid request param at every request.

## ProcessCaching
Dummy caching layer. This is currently caching the data in the lambda process.
TODO: Add an interface and let the user chose is caching layer (Redis, Memcached, etc.)
