const getMockNextMiddleware = () => {
  return (ctx, next) => { next && next() }
}

export default getMockNextMiddleware
