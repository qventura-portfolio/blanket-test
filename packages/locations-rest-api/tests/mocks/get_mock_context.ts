import {Constants} from '../../src/config'
import {Context} from '@qventura-portfolio/common/dist/lambda'

const getMockContext = () : Context => {
  return {
    environment: 'dev',
    serviceName: Constants.serviceName,
    request: {
      queryParams: {
      }
    },
    response: {

    }
  }
}

export default getMockContext
