import {RequiredParameterError} from '@qventura-portfolio/common/dist/errors'

import chai, {expect} from 'chai'
import chaiAsPromised from 'chai-as-promised'
chai.use(chaiAsPromised)

import getAll from '../../../../src/api/v1/locations/get_all'
import {Constants} from '../../../../src/config'
import {getMockContext, getMockNextMiddleware} from '../../../mocks'

describe('Api.V1.Locations.getAll', () => {
  const next = getMockNextMiddleware()
  let context

  before(async () => {
    context = getMockContext()
  })

  describe('When no name are provided', () => {
    before(async () => {
      context.request.queryParams = {}

      await getAll(context, next)
    })

    it('Set the context response statusCode to 200 ', () => {
      expect(context.response.statusCode).to.eql(200)
    })

    it('Set the context response body with an empty array ', () => {
      expect(context.response.body.locations.length).to.eql(0)
    })
  })

  describe('When name are provided', () => {
    before(async () => {
      context.request.queryParams = {
        name: [
          'Statue of liberty',
          'Something that doesn\'t exist',
          'Eiffel Tower'
        ]
      }

      await getAll(context, next)
    })

    it('Set the context response statusCode to 200 ', () => {
      expect(context.response.statusCode).to.eql(200)
    })

    it('Set the context response body with the locations ', () => {
      expect(context.response.body.locations.length).to.eql(2)
      expect(context.response.body.locations[0].inputName).to.eql(context.request.queryParams.name[0])
      expect(context.response.body.locations[1].inputName).to.eql(context.request.queryParams.name[2])
    })
  })

  describe('When an empty name if provided', () => {
    before(async () => {
      context.request.queryParams = {
        name: [
          '',
        ]
      }

      await getAll(context, next)
    })

    it('Set the context response statusCode to 200', () => {
      expect(context.response.statusCode).to.eql(200)
    })

    it('Set the context response body with an empty array', () => {
      expect(context.response.body.locations.length).to.eql(0)
    })
  })

  describe('When only one existing location is provided ', () => {
    before(async () => {
      context.request.queryParams = {
        name: [
          'Statue of liberty',
          'Something that doesn\'t exist'
        ]
      }

      await getAll(context, next)
    })

    it('Set the context response statusCode to 200', () => {
      expect(context.response.statusCode).to.eql(200)
    })

    it('Set the context response body with one location, without nearestLocation being set', () => {
      expect(context.response.body.locations.length).to.eql(1)
      expect(context.response.body.locations[0].inputName).to.eql(context.request.queryParams.name[0])
      expect(context.response.body.locations[0].nearestLocation).to.be.undefined
    })
  })
})
