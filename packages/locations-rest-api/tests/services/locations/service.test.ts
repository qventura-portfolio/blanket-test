import {Context} from '@qventura-portfolio/common/dist/lambda'

import {expect} from 'chai'

import Service from '../../../src/services/locations/service'
import {getMockContext} from '../../mocks'

describe('Services.Locations.Service', () => {
  let result

  describe('getAll', () => {
    before(async () => {
      const context = getMockContext()

      result = await new Service(context).getAll({
        names: [
          'Statue of liberty',
          'Something that doesn\'t exist',
          'Eiffel Tower'
        ]
      })
    })

    describe('When two of the names have corresponding locations and one doesn\'t', () => {
      it('return an array containing exactly the corresponding two locations', () => {
        expect(result.length).to.eql(2)
        expect(result).to.eql(expectedResult)
      })
    })
  })
})

const expectedResult = [
  {
    formattedAddress: "New York, NY 10004, USA",
    geolocation: {
      latitude: 40.6892494,
      longitude: -74.04450039999999
    },
    nearestLocation: {
      distance: 5843952,
      location: {
        formattedAddress: "Champ de Mars, 5 Avenue Anatole France, 75007 Paris, France",
        geolocation: {
          latitude: 48.85837009999999,
          longitude: 2.2944813
        },
        inputName: "Eiffel Tower",
        name: "Eiffel Tower"
      }
    },
    inputName: "Statue of liberty",
    name: "Statue of Liberty National Monument"
  },
  {
    formattedAddress: "Champ de Mars, 5 Avenue Anatole France, 75007 Paris, France",
    geolocation: {
      latitude: 48.85837009999999,
      longitude: 2.2944813
    },
    nearestLocation: {
      distance: 5843952,
      location: {
        formattedAddress: "New York, NY 10004, USA",
        geolocation: {
          latitude: 40.6892494,
          longitude: -74.04450039999999,
        },
        inputName: "Statue of liberty",
        name: "Statue of Liberty National Monument",
      }
    },
    inputName: "Eiffel Tower",
    name: "Eiffel Tower"
  }
]
