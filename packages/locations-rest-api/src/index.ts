import {lambdaHandler, Context} from '@qventura-portfolio/common/dist/lambda'
import * as Api from './api'
import {Constants} from './config'

const serviceName = Constants.serviceName
const environment = process.env.NODE_ENV

const apiV1LocationsGetAll = lambdaHandler({
  environment,
  eventHandler: Api.V1.Locations.getAll,
  serviceName
})

export {
  apiV1LocationsGetAll
}
