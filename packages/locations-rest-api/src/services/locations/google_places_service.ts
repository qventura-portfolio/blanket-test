import GoogleMapsApi from '@google/maps'
import {ConfigManager, ProcessCaching} from '@qventura-portfolio/common/dist/lib'
import {Context} from '@qventura-portfolio/common/dist/lambda'
import Location from './location'

export default class Service {
  private client

  constructor(private ctx: Context) { }

  public async initClient() {
    const key = await ConfigManager.getInstance(this.ctx).getEnvVar('GoogleApiKey')
    this.client = GoogleMapsApi.createClient({ key, Promise: Promise })
  }

  public async getClient() {
    if(!this.client) {
      await this.initClient()
    }

    return this.client
  }

  public async findAllByNames({ names }) : Promise<Location[]> {
    await this.initClient()

    const locations : Location[] =  await Promise.all(
      (names || [])
        .filter((name) => !!name)
        .map(async (name) => await this.findOneByName({ name }))
    )

    return locations.filter((location) => !!location)
  }

  public async findOneByName({ name }) : Promise<Location> {
    return await ProcessCaching.getInstance().getOrRunAndSet(
      `GooglePlacesService.findOneByName.${name}`,
      () => this.findOneByNameFromGooglePlacesAPI({ name })
    )
  }

  public async findOneByNameFromGooglePlacesAPI({ name }) : Promise<Location> {
    const client = await this.getClient()

    const response = await client.findPlace({
      input: name,
      inputtype: 'textquery',
      fields: [
        'formatted_address',
        'geometry',
        'name',
      ],
    }).asPromise()

    const result = response.json.candidates[0]

    if(!result) {
      return
    }

    return {
      inputName: name,
      formattedAddress: result.formatted_address,
      geolocation: {
        latitude: result.geometry && Number(result.geometry.location.lat),
        longitude: result.geometry && Number(result.geometry.location.lng),
      },
      name: result.name
    }
  }
}
