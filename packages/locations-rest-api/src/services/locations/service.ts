import * as geolib from 'geolib'
import GooglePlacesService from './google_places_service'
import Location from './location'

export default class Service {
  constructor(private ctx) { }

  async getAll({ names }) : Promise<Location[]> {
    let locations: Location[] = await new GooglePlacesService(this.ctx)
      .findAllByNames({ names })

    return locations.map((location: Location) => {
      location.nearestLocation = this.findNearest(location, locations)

      return location
    })
  }

  findNearest(location, locations) {
    const distances = locations.map((destLocation) =>
      this.computeDistance(location, destLocation)
    )
    
    let min = Number.MAX_VALUE
    let minIndex = null
    let index = 0

    distances.forEach((distance) => {
      if(distance > 1 && distance < min) {
        min = distance
        minIndex = index
      }

      index++
    })

    if(minIndex == null) {
      return
    }

    const {
      inputName,
      formattedAddress,
      geolocation,
      name,
    } = locations[minIndex]

    return {
      location: {
        inputName,
        formattedAddress,
        geolocation,
        name,
      },
      distance: distances[minIndex]
    }
  }

  computeDistance(location, destLocation) {
    return geolib.getDistance(
      { latitude: location.geolocation.latitude, longitude: location.geolocation.longitude },
      { latitude: destLocation.geolocation.latitude, longitude: destLocation.geolocation.longitude }
    )
  }
}
