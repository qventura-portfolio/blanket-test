interface Geolocation {
  longitude: number
  latitude: number
}

interface NearestLocation {
  distance: number
  location: Location
}

interface Location {
  inputName: string
  formattedAddress: string
  nearestLocation?: NearestLocation
  geolocation: Geolocation
  name: string
}

export default Location
