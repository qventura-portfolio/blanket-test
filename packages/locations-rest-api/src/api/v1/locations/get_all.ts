import {Context} from '@qventura-portfolio/common/dist/lambda'
import {RequiredParameterError} from '@qventura-portfolio/common/dist/errors'
import {Service} from '../../../services/locations'

const getAll = async (ctx: Context, next: Function) => {
  const names = ctx.request.queryParams.name
  const locations = await new Service(ctx).getAll({ names })

  ctx.response.statusCode = 200
  ctx.response.body = {
    locations
  }

  next()
}

export default getAll
