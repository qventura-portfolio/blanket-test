# API Documentation

`GET /api/v1/locations?name=name 1&name=name 2`

## Parameters

The endpoint take an array of `name` as a parameter.
It doesn\'t accept comma separated values.

## Implementation Choices
If no locations is found with the name, it will not appears on the response.

If only one name is provided, or, multiple names are provided but only one as a matching location, the response will contains this only location without `nearestLocation`

If not name are provided, or none have a matching location, the endpoint with return an empty array

## Response
```
{
  locations: Location[]
}
```

## Models
### Location
```
interface Location {
  inputName: string
  formattedAddress: string
  nearestLocation?: NearestLocation
  geolocation: Geolocation
  name: string
}
```

### Geolocation
```
interface Geolocation {
  longitude: number
  latitude: number
}
```

### NearestLocation
```
interface NearestLocation {
  distance: number
  location: Location
}
```


# Known issues
If trying to export a Postman snippet and run it as a CURL command, the working example is exported with the values separated by commas.
